import math

PENDING         = 0   # The goal has yet to be processed by the action server
ACTIVE          = 1   # The goal is currently being processed by the action server
PREEMPTED       = 2   # The goal received a cancel request after it started executing
                      #   and has since completed its execution (Terminal State)
SUCCEEDED       = 3   # The goal was achieved successfully by the action server (Terminal State)
ABORTED         = 4   # The goal was aborted during execution by the action server due
                      #    to some failure (Terminal State)
REJECTED        = 5   # The goal was rejected by the action server without being processed,
                      #    because the goal was unattainable or invalid (Terminal State)
PREEMPTING      = 6   # The goal received a cancel request after it started executing
                      #    and has not yet completed execution
RECALLING       = 7   # The goal received a cancel request before it started executing,
                      #    but the action server has not yet confirmed that the goal is canceled
RECALLED        = 8   # The goal received a cancel request before it started executing
                      #    and was successfully cancelled (Terminal State)
LOST            = 9   # An action client can determine that a goal is LOST. This should not be
                      #    sent over the wire by an action server

def quaternion_to_euler(x, y, z, w):

    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    X = math.degrees(math.atan2(t0, t1))

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    Y = math.degrees(math.asin(t2))

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    Z = math.degrees(math.atan2(t3, t4))

    return X, Y, Z

