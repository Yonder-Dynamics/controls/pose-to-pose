#! /usr/bin/env python

import rospy
import math

import actionlib
import numpy as np

from move_base_msgs.msg import MoveBaseActionResult, MoveBaseActionGoal, MoveBaseAction, MoveBaseActionFeedback
from geometry_msgs.msg import PoseStamped, Pose
from std_msgs.msg import Float64MultiArray

from rover import Rover
import utils


class PoseToPoseAction(object):
    # create messages that are used to publish feedback/result
    #_feedback = MoveBaseActionFeedback()
    _result = MoveBaseActionResult()

    def __init__(self, name, rover):
        self._action_name = name
        self._pose = None
        self._as = actionlib.SimpleActionServer(
            self._action_name, MoveBaseAction,
            execute_cb=self.execute_cb, auto_start = False)
        self._rover = rover
        self._dt_pub = rospy.Publisher(
            'autonomous_drive_train', Float64MultiArray, queue_size=10)
        sub = rospy.Subscriber('zed/zed_node/pose', PoseStamped, self.pose_update_callback, queue_size=10)
        print(sub)
        self._as.start()

    def pose_update_callback(self, msg):
        self._pose = msg.pose
      
    def execute_cb(self, goal_msg):
        print("YO I GOT A FUCKIN GOAL")
        # helper variables
        r = rospy.Rate(10)
        success = False
        print(goal_msg)

        if self._pose is None:
            self._result.status.status = utils.REJECTED
            rospy.loginfo('%s: Pose not found' % self._action_name)
            self._as.set_succeeded(self._result)
            return

        goal_pose = goal_msg.target_pose.pose
        (_, _, goal_theta) = utils.quaternion_to_euler(
                goal_pose.orientation.x,
                goal_pose.orientation.y,
                goal_pose.orientation.z,
                goal_pose.orientation.w)
        goal_pos = np.array([goal_pose.position.x, goal_pose.position.y])

        
        # start executing the action
        dist = 1
        while dist > 0.1:
            # check that preempt has not been requested by the client
            if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
                break

            # Construct rover_pos and rover_theta
            (_, _, rover_theta) = utils.quaternion_to_euler(
                    self._pose.orientation.x,
                    self._pose.orientation.y,
                    self._pose.orientation.z,
                    self._pose.orientation.w)
            rover_pos = np.array([self._pose.position.x, self._pose.position.y])

            # Calculate command
            [vL, vR], dist = self._rover.get_current_cmd(
                rover_pos, rover_theta, goal_pos, goal_theta)
            
            print(vL,vR, dist, 180 / math.pi * math.atan2(goal_pos[1] - rover_pos[1], goal_pos[0] - rover_pos[0]) - rover_theta, rover_theta)
            if dist < 0.5:
                print("I SUCCEEDED")
                success = True

            # Publish drive train stuff
            msg = Float64MultiArray()
            msg.data = [-vL, -vR]
            self._dt_pub.publish(msg)

            # publish the feedback
            #self._feedback.feedback.base_position.pose = self._pose
            #self._feedback.status.status = utils.PENDING
            #self._as.publish_feedback(self._feedback)
            r.sleep()
          
        if success:
            self._result.status.status = utils.SUCCEEDED
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('move_base')
    rover = Rover()
    server = PoseToPoseAction(rospy.get_name(), rover)
    rospy.spin()
