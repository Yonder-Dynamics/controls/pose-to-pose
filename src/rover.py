import utils
import numpy as np

R = .5

class Rover:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.theta = 0

        # weightings for curving of path
        self.Kp_alpha = 15
        self.Kp_beta = -6

        # rover parameters
        self.wheel_center_distance = 5.5
        self.speed = 30 #out of 255 (too high will take out of bounds during turn)

    def unicycle_to_differential(self, v, omega):
        # v = translational velocity (m/s)
        # omega = angular velocity (rad/s)
        vL = ((2.0 * v) - (omega * self.wheel_center_distance)) / (2.0 * R)
        vR = ((2.0 * v) + (omega * self.wheel_center_distance)) / (2.0 * R)
        return vL, vR

    def cancel_goal(self, msg):
        self.goal_server.set_aborted()

    def get_current_cmd(self, rover_pos, rover_theta, goal_pos, goal_theta):

        x_diff = goal_pos[0] - rover_pos[0]
        y_diff = goal_pos[1] - rover_pos[1]
        # Restrict alpha and beta (angle differences) to the range
        # [-pi, pi] to prevent unstable behavior e.g. difference going
        # from 0 rad to 2*pi rad with slight turn

        dist = np.hypot(x_diff, y_diff)
        alpha = (np.arctan2(y_diff, x_diff)
                - rover_theta + np.pi) % (2 * np.pi) - np.pi

        #ignoring the orienation term for now
        #beta = (goal_theta - rover_theta - alpha + np.pi) % (2 * np.pi) - np.pi
        beta = 0

        v = self.speed
        w = self.Kp_alpha * alpha + self.Kp_beta * beta

        if alpha > np.pi / 2 or alpha < -np.pi / 2:
            v = -v

        vL, vR = self.unicycle_to_differential(v, w)

        if(vR - vL == 0):
            vR += .00001

        return [vL, vR], dist

    def predict_next_pose(self, rover_pos, rover_theta, vL, vR, dt):

        r = self.wheel_center_distance/2 * (vR + vL) / (vR - vL)
        omega = (vR - vL) / self.wheel_center_distance

        x_icc = rover_pos[0] - r * np.sin(rover_theta)
        y_icc = rover_pos[1] + r * np.cos(rover_theta)


        #  dt = 1.0/hertz
        rover_theta = rover_theta + omega * dt

        x = np.cos(omega * dt) * (rover_pos[0] - x_icc) - np.sin(omega * dt) * (rover_pos[1] - y_icc) + x_icc
        y = np.sin(omega * dt) * (rover_pos[0] - x_icc) + np.cos(omega * dt) * (rover_pos[1] - y_icc) + y_icc

        return np.array([x, y])
