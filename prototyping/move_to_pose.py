import matplotlib.pyplot as plt
import numpy as np
from random import random

# simulation parameters
speed = 30
Kp_alpha = 15
Kp_beta = -6
dt = 0.024 / 10
R = .5

show_animation = True

L = 4
# distance between wheel centers

def unicycletodifferential(v, omega):
    # v = translational velocity (m/s)
    # omega = angular velocity (rad/s)
    vL = ((2.0 * v) - (omega * L)) / (2.0 * R)
    vR = ((2.0 * v) + (omega * L)) / (2.0 * R)
    return vL, vR


def move_to_pose(x_start, y_start, theta_start, x_goal, y_goal, theta_goal):
    """
    rho is the distance between the robot and the goal position
    alpha is the angle to the goal relative to the heading of the robot
    beta is the angle between the robot's position and the goal position plus the goal angle

    speed*rho and Kp_alpha*alpha drive the robot along a line towards the goal
    Kp_beta*beta rotates the line so that it is parallel to the goal angle
    """
    x = x_start
    y = y_start
    theta = theta_start

    x_diff = x_goal - x
    y_diff = y_goal - y

    x_traj, y_traj = [], []

    rho = np.hypot(x_diff, y_diff)

    counter = 0
    while rho > 1:
        x_traj.append(x)
        y_traj.append(y)

        x_diff = x_goal - x
        y_diff = y_goal - y

        # Restrict alpha and beta (angle differences) to the range
        # [-pi, pi] to prevent unstable behavior e.g. difference going
        # from 0 rad to 2*pi rad with slight turn

        rho = np.hypot(x_diff, y_diff)
        alpha = (np.arctan2(y_diff, x_diff)
                 - theta + np.pi) % (2 * np.pi) - np.pi
        # beta = (theta_goal - theta - alpha + np.pi) % (2 * np.pi) - np.pi
        beta = 0

        v = speed
        w = Kp_alpha * alpha + Kp_beta * beta

        if alpha > np.pi / 2 or alpha < -np.pi / 2:
            v = -v


        vL, vR = unicycletodifferential(v, w)
        # d_theta = -np.arctan2((vL - vR) * dt, L)

        print(vL, vR)
        # print(d_theta)
        # theta += d_theta
        # x += np.cos(theta) * dt * v
        # y += np.sin(theta) * dt * v
        

        r = L/2 * (vR + vL) / (vR - vL)
        omega = (vR - vL) / L


        x_icc = x - r * np.sin(theta)
        y_icc = y + r * np.cos(theta)
        # theta = theta + d_theta
        theta = theta + omega * dt
        x = np.cos(omega * dt) * (x - x_icc) - np.sin(omega * dt) * (y - y_icc) + x_icc
        y = np.sin(omega * dt) * (x - x_icc) + np.cos(omega * dt) * (y - y_icc) + y_icc
        
        counter += 1


        if show_animation:  # pragma: no cover
            plt.cla()
            plt.arrow(x_start, y_start, np.cos(theta_start),
                      np.sin(theta_start), color='r', width=0.1)
            plt.arrow(x_goal, y_goal, np.cos(theta_goal),
                      np.sin(theta_goal), color='g', width=0.1)
            plot_vehicle(x, y, theta, x_traj, y_traj)

    return x,y,theta

def plot_vehicle(x, y, theta, x_traj, y_traj):  # pragma: no cover
    # Corners of triangular vehicle when pointing to the right (0 radians)
    p1_i = np.array([0.5, 0, 1]).T
    p2_i = np.array([-0.5, 0.25, 1]).T
    p3_i = np.array([-0.5, -0.25, 1]).T

    T = transformation_matrix(x, y, theta)
    p1 = np.matmul(T, p1_i)
    p2 = np.matmul(T, p2_i)
    p3 = np.matmul(T, p3_i)

    plt.plot([p1[0], p2[0]], [p1[1], p2[1]], 'k-')
    plt.plot([p2[0], p3[0]], [p2[1], p3[1]], 'k-')
    plt.plot([p3[0], p1[0]], [p3[1], p1[1]], 'k-')

    plt.plot(x_traj, y_traj, 'b--')

    # for stopping simulation with the esc key.
    plt.gcf().canvas.mpl_connect('key_release_event',
            lambda event: [exit(0) if event.key == 'escape' else None])

    plt.xlim(-10, 10)
    plt.ylim(-10, 10)

    plt.pause(dt)


def transformation_matrix(x, y, theta):
    return np.array([
        [np.cos(theta), -np.sin(theta), x],
        [np.sin(theta), np.cos(theta), y],
        [0, 0, 1]
    ])


def main():

    x_start = 0
    y_start = 0
    theta_start = 0
    x_goal = 4
    y_goal = 4
    theta_goal = np.pi /2
    x_start, y_start, theta_start = move_to_pose(x_start, y_start, theta_start, x_goal, y_goal, theta_goal)
    x_goal = 0
    y_goal = 8
    theta_goal = np.pi 
    x_start, y_start, theta_start = move_to_pose(x_start, y_start, theta_start, x_goal, y_goal, theta_goal)
    x_goal = -4
    y_goal = 4
    theta_goal = np.pi * 3/2
    x_start, y_start, theta_start = move_to_pose(x_start, y_start, theta_start, x_goal, y_goal, theta_goal)
    x_goal = 0
    y_goal = 0
    theta_goal = 0
    x_start, y_start, theta_start = move_to_pose(x_start, y_start, theta_start, x_goal, y_goal, theta_goal)



    # print("Initial x: %.2f m\nInitial y: %.2f m\nInitial theta: %.2f rad\n" %
    #         (x_start, y_start, theta_start))
    # print("Goal x: %.2f m\nGoal y: %.2f m\nGoal theta: %.2f rad\n" %
    #         (x_goal, y_goal, theta_goal))


if __name__ == '__main__':
    main()
