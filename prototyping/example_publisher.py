import rospy
import numpy as np
import time
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose, PoseArray
import sys

if(len(sys.argv) != 4):
    print("expects an x,y,theta")

print(sys.argv)

def euler_to_quaternion(yaw, pitch, roll):

    qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)

    return qx, qy, qz, qw

num = int(sys.argv[1])
index = 2
pose_list = []
for i in range(num):
    pose = Pose()
    x = float(sys.argv[index])
    y = float(sys.argv[index + 1])
    pose.position.x = x
    pose.position.y = y
    theta = float(sys.argv[index + 2])

    x,y,z,w = euler_to_quaternion(theta,0,0)
    pose.orientation.x = x
    pose.orientation.y = y
    pose.orientation.z = z
    pose.orientation.w = w
    index += 3
    pose_list.append(pose)

rospy.init_node("testing_publisher")

time.sleep(1)
poseArray = PoseArray()
poseArray.poses = pose_list
path_publisher = rospy.Publisher('rover_path', PoseArray, queue_size=1)


time.sleep(1)
# for i in range(3):
path_publisher.publish(poseArray)
